PROGRAM array_lookup
  USE link_list
  ! USE hash_table
  IMPLICIT NONE

  INTEGER :: num, i, j, checksum
  REAL :: chk, rv, time1, time2
  INTEGER, ALLOCATABLE, DIMENSION(:) :: idx
  TYPE(pair),ALLOCATABLE,DIMENSION(:) :: dat
  TYPE(pair) :: p, p1
  TYPE(node), POINTER:: head => NULL()
  INTEGER, PARAMETER :: nlook = 5000



  p%key = 0
  p%val = 0.0

  READ(5,*) num
  ALLOCATE(dat(num))
  READ(5,*) (dat(i),i=1,num)
  READ(5,*) chk

  ! XXX fill linked list or hash table with items from dat() here
  ! CALL list_begin(head)
  DO i = 1, num
     CALL push_front(head, dat(i))
  END DO

  ! fill idx array with randomly selected keys
  CALL RANDOM_SEED()
  ALLOCATE(idx(nlook))
  DO i=1,nlook
     CALL RANDOM_NUMBER(rv)
     j = INT(rv*num)+1
     idx(i) = dat(j)%key
  END DO

  CALL CPU_TIME(time1)
  checksum = 0
  DO i=1,nlook
     DO j=1,num
        IF (dat(j)%key == idx(i)) THEN
           p = dat(j)
           EXIT
        END IF
     END DO
     checksum = checksum + p%key
  END DO
  CALL CPU_TIME(time2)
  WRITE(*,FMT=666) nlook, 'array value lookups', (time2-time1)*1000.0
  WRITE(*,*) "Checksum:", checksum

  CALL CPU_TIME(time1)
  checksum = 0
  DO i=1,nlook
     p1 = find(head, idx(i))
     checksum = checksum + p1%key
  END DO
  CALL CPU_TIME(time2)
  WRITE(*,FMT=666) nlook, 'Linked list lookups', (time2-time1)*1000.0
  WRITE(*,*) "Checksum:", checksum

  CALL delete(head)
  ! DEALLOCATE(dat,idx)
666 FORMAT (' Performing',I8,1X,A20,1X,'took:',F12.6,' ms')
END PROGRAM array_lookup
