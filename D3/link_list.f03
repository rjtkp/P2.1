MODULE link_list
  IMPLICIT NONE

  TYPE pair
     INTEGER(4) :: key
     REAL(4) :: val
  END TYPE pair

  TYPE node
     TYPE(pair) :: DATA
     TYPE( node ), POINTER :: next
  END TYPE node

CONTAINS

  SUBROUTINE push_front(head, pair1)
    IMPLICIT NONE
    TYPE(node), INTENT(inout), POINTER:: head
    TYPE(pair), INTENT(in):: pair1
    TYPE(node), POINTER:: tmp

    ALLOCATE(tmp)
    tmp%DATA = pair1
    tmp%next => head
    head => tmp

  END SUBROUTINE push_front

  SUBROUTINE printnode(DATA)
    TYPE(pair), INTENT(in)  :: DATA
    WRITE (*,*) DATA%key, DATA%val
  END SUBROUTINE printNode

  TYPE(pair) FUNCTION find (head, key1)
    IMPLICIT NONE
    TYPE(node), INTENT(in), POINTER:: head
    INTEGER, INTENT(in):: key1
    TYPE(node), POINTER :: buf
    LOGICAL :: is_associated

    buf => head
    is_associated = .FALSE.
    DO WHILE (ASSOCIATED(buf))
       IF (buf%DATA%key == key1) THEN
          find = buf%DATA
          is_associated = .TRUE.
          ! CALL printnode(find)
          EXIT
       END IF
       buf => buf%next
    END DO

    IF (is_associated .EQV. .FALSE.) THEN
       WRITE(*,*) "The key is not present in the list :("
    END IF
  END FUNCTION  find

  SUBROUTINE delete(head)
    IMPLICIT NONE
    TYPE(node), INTENT(inout), POINTER :: head
    TYPE(node), POINTER :: buf

    DO WHILE(ASSOCIATED(head) .EQV. .TRUE.)
       buf => head%next
       DEALLOCATE(head)
       head => buf
    END DO
  END SUBROUTINE delete

END MODULE link_list
