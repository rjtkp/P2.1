PROGRAM test_stack_llist
  USE link_list
  USE stack_ll_mod
  IMPLICIT NONE

  TYPE(pair) :: item
  TYPE(stack_ll), POINTER :: stk1, stk2
  INTEGER :: i, n, num
  REAL(4) :: time1, time2
  INTEGER,PARAMETER,DIMENSION(9) :: sizes = (/ &
       500,1000,2000,5000,10000,20000,50000,100000,200000 /)

  DO n=1,SIZE(sizes,1)
     num = sizes(n)
     ALLOCATE(stk1)
     stk1 = stack_ll()


     CALL cpu_TIME(time1)
     DO i = 1, num
        item%key = i
        item%val = 1.0
        CALL stk1%push(item)
     END DO
     CALL cpu_TIME(time2)
     WRITE(*,*)"time taken for creating", num, "nodes=", (time2-time1)

     CALL cpu_TIME(time1)
     ALLOCATE(stk2)
     stk2 = stack_ll(stk1)
     CALL cpu_TIME(time2)
     WRITE(*,*)"time taken to copy", num, "nodes=", (time2-time1)

     ! add more data to both
     DO i = 1, 1
        item%key = i + 100
        item%val = 1.0
        CALL stk1%push(item)
        !CALL sa2%push(item)
     END DO

     ! empty both stacks
     DO i = stk1%length(), 1, -1
        item = stk1%pop()
     END DO

     DO i = stk2%length(), 1, -1
        item = stk2%pop()
     END DO

     ! free
     CALL stk1%free()
     CALL stk2%free()
     DEALLOCATE(stk1)
     DEALLOCATE(stk2)
  END DO
END PROGRAM test_stack_llist
