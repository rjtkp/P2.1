PROGRAM test_node
  USE nodes
  IMPLICIT NONE
  TYPE(node),POINTER :: head,nn,ptr
  TYPE(node),TARGET :: n
  INTEGER :: i

  ALLOCATE(head,nn)
  ! initialize class instances with different overloaded constructors
  head = node(1)
  nn = node()
  n = node()
  ptr => n
  ! make use of instances
  CALL n%set(20)
  CALL head%append(nn)
  CALL head%append(ptr)

  PRINT*,'val=', head%get(), n%get(), nn%get()

  ! loop through list from the head
  ptr => head
  i = 1
  DO WHILE (ASSOCIATED(ptr))
     PRINT*,i,' -> ',ptr%get()
     ptr => ptr%next
     i = i+1
  END DO

  ! need to free dynamically allocated storage
  DEALLOCATE(head,nn)
  PRINT*,'done'
END PROGRAM test_node
