PROGRAM test_stackrray
  USE array_stack
  IMPLICIT NONE

  TYPE(stack) :: stk1, stk2
  INTEGER(4) :: pop1, pop2, i, n, num
  REAL(4) :: time1, time2
  INTEGER,PARAMETER,DIMENSION(9) :: sizes = (/ &
       500,1000,2000,5000,10000,20000,50000,100000,200000 /)

  DO n=1,SIZE(sizes,1)
     num = sizes(n)
     stk1 = stack(num)

     CALL cpu_TIME(time1)
     DO i = 1, num
        CALL stk1%push(i)
     END DO
     CALL cpu_TIME(time2)

     WRITE(*,*)"time taken for creating", num, "nodes=", (time2-time1)

     CALL cpu_TIME(time1)
     stk2 = stack(stk1)
     CALL cpu_TIME(time2)

     WRITE(*,*)"time taken to copy", num, "nodes=", (time2-time1)

     CALL stk1%push(70)
     CALL stk1%push(836)
     CALL stk2%push(4030)
     CALL stk2%push(5830)

     DO WHILE (stk1%length() .NE. 0)
        pop1 = stk1%pop()
     END DO
     DO WHILE (stk2%length() .NE. 0)
        pop2 = stk2%pop()
     END DO

     CALL free(stk1)
     CALL free(stk2)
  END DO
END PROGRAM test_stackrray
