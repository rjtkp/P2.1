MODULE stack_ll_mod
  ! USE list_types
  USE link_list
  IMPLICIT NONE

  PRIVATE

  TYPE stack_ll
     TYPE(node), POINTER :: head
     INTEGER :: info

   CONTAINS

     PROCEDURE :: free
     PROCEDURE :: push
     PROCEDURE :: pop
     PROCEDURE :: length

  END TYPE stack_ll

  INTERFACE stack_ll
     MODULE PROCEDURE stack_ll_default ! the deafault constructr
     MODULE PROCEDURE stack_ll_copy     ! the copy constructor
  END INTERFACE stack_ll

  PUBLIC :: stack_ll, free

CONTAINS
  TYPE(stack_ll) FUNCTION stack_ll_default()
    stack_ll_default%info = 0
    ALLOCATE(stack_ll_default%head)
    CALL default(stack_ll_default%head)
  END FUNCTION stack_ll_default

  TYPE(stack_ll) FUNCTION stack_ll_copy(to_copy)
    TYPE(stack_ll), INTENT(IN) :: to_copy
    stack_ll_copy%info = to_copy%info
    ALLOCATE(stack_ll_copy%head)
    stack_ll_copy%head%next => NULL()
    CALL copy(to_copy%head, stack_ll_copy%head)
  END FUNCTION stack_ll_copy

  SUBROUTINE default(head)
    IMPLICIT NONE
    TYPE(node), POINTER, INTENT(INOUT) :: head
    head%DATA%key = 0
    head%DATA%val = 0.0
    head%next => NULL()
  END SUBROUTINE default

  SUBROUTINE copy(head1 , head)
    IMPLICIT NONE
    TYPE(node), POINTER, INTENT(IN) :: head1
    TYPE(node), POINTER, INTENT(INOUT) :: head
    TYPE(node), POINTER :: buf
    TYPE(pair) :: p
    buf => head1
    DO WHILE (ASSOCIATED(buf))
       p%key = buf%DATA%key
       p%val = buf%DATA%val
       CALL push_front(head, p)
       buf => buf%next
    END DO
  END SUBROUTINE copy

  SUBROUTINE free(self)
    CLASS(stack_ll) :: self
    CALL delete(self%head)
  END SUBROUTINE free

  SUBROUTINE push(self, p)
    CLASS(stack_ll) :: self
    TYPE(pair), INTENT(IN) :: p
    self%info = self%info + 1
    CALL push_front(self%head, p)
  END SUBROUTINE push


  TYPE(pair) FUNCTION pop(self)
    CLASS(stack_ll) :: self
    IF (self%info > 0) THEN
       self%info = self%info - 1
       pop = pop_linklist(self%head)
    ELSE
       pop%key = 0
       pop%val = 0.0
    END IF
  END FUNCTION pop

  TYPE(pair) FUNCTION pop_linklist(head)
    TYPE(node), POINTER, INTENT(INOUT) :: head
    TYPE(node), POINTER :: buf
    IF (ASSOCIATED(head)) THEN
       pop_linklist%key = head%DATA%key
       pop_linklist%val = head%DATA%val
       buf => head
       head => head%next
       DEALLOCATE(buf)
    ELSE
       pop_linklist%key = 0
       pop_linklist%val = 0.0
    END IF
  END FUNCTION pop_linklist

  INTEGER FUNCTION length(self)
    CLASS(stack_ll) :: self
    length = self%info
  END FUNCTION length

END MODULE stack_ll_mod
