MODULE list_types
  IMPLICIT NONE
  TYPE pair
     INTEGER(4) :: key
     REAL(4) :: val
  END TYPE pair

END MODULE list_types
