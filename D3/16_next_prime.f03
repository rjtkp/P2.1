PROGRAM test_prime
  USE list_tools
  IMPLICIT NONE
  INTEGER :: n, prime

  PRINT *, "Enter number:"
  READ *, n

  prime = next_prime(n)

  PRINT *, prime

END PROGRAM test_prime
