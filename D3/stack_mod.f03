MODULE array_stack
  USE list_types
  IMPLICIT NONE


  PRIVATE

  TYPE stack
     INTEGER, POINTER :: stack_arr(:)
     INTEGER :: first
     INTEGER :: max

   CONTAINS
     PROCEDURE :: length
     PROCEDURE :: push
     PROCEDURE :: pop
  END TYPE stack

  INTERFACE stack
     MODULE PROCEDURE stack_default   ! Default constructor
     MODULE PROCEDURE stack_copy        ! the Copy constructor
  END INTERFACE stack

  PUBLIC :: stack, free

CONTAINS

  TYPE(stack) FUNCTION stack_default(size)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: size

    ALLOCATE(stack_default%stack_arr(size))

    stack_default%first = 0
    stack_default%max = size

  END FUNCTION stack_default

  SUBROUTINE free(stk)
    IMPLICIT NONE
    TYPE(stack), INTENT(inout) :: stk

    DEALLOCATE(stk%stack_arr)

  END SUBROUTINE free

  TYPE(stack)  FUNCTION stack_copy(to_copy)
    IMPLICIT NONE
    TYPE(stack), INTENT(in) :: to_copy
    INTEGER :: i

    ALLOCATE(stack_copy%stack_arr(to_copy%max))

    DO i = 1, to_copy%first
       stack_copy%stack_arr(i) = to_copy%stack_arr(i)
    END DO

    stack_copy%first = to_copy%first
    stack_copy%max = to_copy%max

  END FUNCTION stack_copy

  INTEGER FUNCTION length(self)
    CLASS(stack) :: self
    length = self%first
  endfunction length

  SUBROUTINE push(self, p)
    IMPLICIT NONE
    CLASS(stack), INTENT(inout) :: self
    INTEGER, INTENT(in):: p
    TYPE(stack) :: buf

    IF (self%first == self%max) THEN
       self%max = self%max*2
       buf = stack_copy(self)
       DEALLOCATE(self%stack_arr)
       self%stack_arr => buf%stack_arr
    END IF

    self%first = self%first + 1
    self%stack_arr(self%first) = p

  END SUBROUTINE push

  INTEGER  FUNCTION pop(self)
    IMPLICIT NONE
    CLASS(stack), INTENT(inout) :: self

    IF (self%first == 0) THEN
       pop = -1
       RETURN
    END IF
    pop = self%stack_arr(self%first)
    self%first = self%first - 1
  END FUNCTION pop

END MODULE array_stack
