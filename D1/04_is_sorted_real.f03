PROGRAM four
  USE list_tools
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 4
  REAL (KIND=dp), ALLOCATABLE :: t(:)
  INTEGER (KIND=4):: n

  READ(5,*) n         !reading the number of integers
  WRITE(*,*) "Total number of integers", n
  ALLOCATE(t(n))
  READ(5,*)t          ! so, read them into the array t     DEALLOCATE(t)
  IF (is_sorted_real(t)) THEN
     WRITE(*,*) "The array is sorted."
  ELSE
     WRITE(*,*) "The array is not sorted"
  ENDIF

  WRITE(*,*) "array is sorted ascending", is_sorted_real(t,.TRUE.)
  WRITE(*,*) "array is sorted descending", is_sorted_real(t,.FALSE.)
  DEALLOCATE(t)

END PROGRAM four
