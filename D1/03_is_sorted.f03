PROGRAM three
  USE list_tools
  IMPLICIT NONE
  INTEGER, PARAMETER :: sp =4
  INTEGER(4), ALLOCATABLE :: t(:)
  INTEGER (KIND=sp):: n

  ! INTERFACE
  !    LOGICAL FUNCTION is_sorted(arr)
  !      INTEGER(4), ALLOCATABLE, INTENT(IN):: arr(:)
  !   END INTERFACE


  READ(5,*) n         !reading the number of integers
  WRITE(*,*) "Total number of integers", n
  ALLOCATE(t(n))
  READ(5,*)t          ! so, read them into the array t     DEALLOCATE(t)
  IF (is_sorted(t)) THEN
     WRITE(*,*) "The array is sorted."
  ELSE
     WRITE(*,*) "The array is not sorted"
  ENDIF
  WRITE(*,*) "The array asending", is_sorted(t)
  DEALLOCATE(t)

END PROGRAM three

LOGICAL FUNCTION is_sorted(arr)
  IMPLICIT NONE

  INTEGER, ALLOCATABLE, INTENT(IN):: arr(:)
  INTEGER :: i

  DO i =1,SIZE(arr)-1
     IF (arr(i) .GT. arr(i+1)) THEN
        is_sorted  = .FALSE.
        EXIT
     ELSE
        is_sorted  = .TRUE.
     ENDIF
  ENDDO
END FUNCTION is_sorted
