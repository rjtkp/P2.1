PROGRAM readfloat
  IMPLICIT NONE

  INTEGER, PARAMETER :: sp =4
  REAL (KIND= sp), ALLOCATABLE :: t(:)
  REAL (KIND= sp):: checksum , my_sum, tolerance,x  ! the real is 4byte long but has been convert to double presicion type with kind =8
  INTEGER (KIND=sp):: n

  tolerance =  0.00001_4

  READ(5,*) n         !reading the number of integers
  WRITE(*,*) "Total number of integers", n
  ALLOCATE(t(n))
  READ(5,*)t          ! so, read them into the array t
  READ(5,*) checksum  ! the provided sum of the numbers

  my_sum = SUM(t)
  x = ABS(my_sum - checksum)
  IF (x .LE. tolerance) THEN
     WRITE(*,*) "my_sum:",my_sum ,"  =  ","givensum:", checksum
     WRITE(*,*) "The sum is correct :)"
     WRITE(*,*) "=============================================="
  ELSE
     WRITE(*,*) "the sum is not matching. The absolute difference is:", x
  ENDIF

  DEALLOCATE(t)

END PROGRAM readfloat
