PROGRAM readint
  IMPLICIT NONE

  ! CHARACTER(len = *), PARAMETER:: folder ="./data/"
  ! CHARACTER(len = 20):: filename
  INTEGER, PARAMETER :: sp =4
  INTEGER, ALLOCATABLE :: t(:)
  INTEGER (KIND=sp):: checksum , n, my_sum

  READ(5,*) n         !reading the number of integers
  WRITE(*,*) "Total number of integers", n
  ALLOCATE(t(n))
  READ(5,*)t          ! so, read them into the array t
  READ(5,*) checksum  ! the provided sum of the numbers

  my_sum = SUM(t)
  IF (my_sum .EQ. checksum) THEN
     WRITE(*,*) "my_sum:",my_sum ,"  =  ","givensum:", checksum
     WRITE(*,*) "The sum is matching :)"
     WRITE(*,*) "=============================================="
  ENDIF

  DEALLOCATE(t)

END PROGRAM readint
