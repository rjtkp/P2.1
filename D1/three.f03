PROGRAM one
  IMPLICIT NONE
  INTEGER, PARAMETER :: dp = 8, sp =4
  INTEGER, ALLOCATABLE :: t(:)
  INTEGER (KIND=sp):: n
  INTEGER (KIND=sp):: i
  ! LOGICAL :: is_sorted = .TRUE.

  OPEN(UNIT=11, FILE="./data/d1_1.dat")
  OPEN(UNIT=12, FILE="./data/d1_2.dat")
  OPEN(UNIT=13, FILE="./data/d1_3.dat")
  OPEN(UNIT=14, FILE="./data/d1_4.dat")
  OPEN(UNIT=15, FILE="./data/d1_5.dat")
  OPEN(UNIT=16, FILE="./data/d1_6.dat")

  DO i=11,16
     READ(i,*) n !reading the number of integers
     WRITE(*,*) "Total number of integers", n
     ALLOCATE( t(n) )
     READ(i,*)t          ! so, read them in
     WRITE(*,*) is_sorted(t,n)
     DEALLOCATE(t)
     CLOSE(i)
  ENDDO

CONTAINS
  LOGICAL FUNCTION is_sorted(array, size)
    IMPLICIT NONE

    INTEGER, PARAMETER :: dp = 8, sp =4
    INTEGER , INTENT(IN):: size
    INTEGER, ALLOCATABLE, INTENT(IN):: array(:)
    INTEGER :: i

    DO i =1,size-1
       IF (array(i) .GT. array(i+1)) THEN
          is_sorted  = .FALSE.
          EXIT
       ELSE
          is_sorted  = .TRUE.
       ENDIF
    ENDDO
  END FUNCTION is_sorted

END PROGRAM one
