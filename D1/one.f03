PROGRAM one
  IMPLICIT NONE
  INTEGER, PARAMETER :: sp =4
  INTEGER, ALLOCATABLE :: t(:)
  INTEGER (KIND=sp):: checksum , n, my_sum
  INTEGER (KIND=sp):: i

  OPEN(UNIT=11, FILE="./data/d1_1.dat")
  OPEN(UNIT=12, FILE="./data/d1_2.dat")
  OPEN(UNIT=13, FILE="./data/d1_3.dat")
  OPEN(UNIT=14, FILE="./data/d1_4.dat")
  OPEN(UNIT=15, FILE="./data/d1_5.dat")
  OPEN(UNIT=16, FILE="./data/d1_6.dat")

  DO i=11,16
     READ(i,*) n !reading the number of integers
     WRITE(6,*) "Total number of integers", n
     ALLOCATE( t(N) )
     READ(i,*)t          ! so, read them in
     READ(i,*) checksum
     my_sum = SUM(t)
     IF (my_sum .EQ. checksum) THEN
        WRITE(6,*) "my_sum:",my_sum ,"  =  ","givensum:", checksum
        WRITE(6,*) "It's working :)"
        WRITE(6,*) "=============================================="
     ENDIF
     DEALLOCATE(t)
     CLOSE(i)
  ENDDO

END PROGRAM one
