MODULE list_tools
  USE list_types
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp1 = 4
  REAL (KIND=dp1), ALLOCATABLE :: t1(:)
  LOGICAL, PARAMETER :: ascending = .TRUE., descending = .FALSE.
  TYPE(pair),ALLOCATABLE, DIMENSION(:) :: t2(:)

  PUBLIC is_sorted_real, is_sorted_int

  INTERFACE is_sorted
     MODULE PROCEDURE is_sorted_real, is_sorted_int, is_sorted_pair
  endinterface is_sorted

CONTAINS

  LOGICAL FUNCTION is_sorted_real(array1 , opt_logical)
    IMPLICIT NONE

    REAL (KIND=dp1), INTENT(IN) :: array1(:)
    LOGICAL, OPTIONAL, INTENT(IN) :: opt_logical
    INTEGER :: i

    IF (.NOT. PRESENT(opt_logical) .OR. opt_logical) THEN
       DO i =1,SIZE(array1)-1
          IF (array1(i) .GT. array1(i+1)) THEN
             is_sorted_real  = .FALSE.
             EXIT
          ELSE
             is_sorted_real  = .TRUE.
          ENDIF
       ENDDO
    ELSE
       DO i =1,SIZE(array1)-1
          IF (array1(i) .LT. array1(i+1)) THEN
             is_sorted_real  = .FALSE.
             EXIT
          ELSE
             is_sorted_real  = .TRUE.
          ENDIF
       ENDDO
    ENDIF
  END FUNCTION is_sorted_real

  LOGICAL FUNCTION is_sorted_int(array1 , opt_logical)
    IMPLICIT NONE

    INTEGER (4),INTENT(IN) :: array1(:)
    LOGICAL, OPTIONAL, INTENT(IN) :: opt_logical
    INTEGER :: i

    IF (.NOT. PRESENT(opt_logical) .OR. opt_logical) THEN
       DO i =1,SIZE(array1)-1
          IF (array1(i) .GT. array1(i+1)) THEN
             is_sorted_int  = .FALSE.
             EXIT
          ELSE
             is_sorted_int  = .TRUE.
          ENDIF
       ENDDO
    ELSE
       DO i =1,SIZE(array1)-1
          IF (array1(i) .LT. array1(i+1)) THEN
             is_sorted_int  = .FALSE.
             EXIT
          ELSE
             is_sorted_int  = .TRUE.
          ENDIF
       ENDDO
    ENDIF
  END FUNCTION is_sorted_int

  LOGICAL FUNCTION is_sorted_pair(array1,opt1,opt2)
    IMPLICIT NONE
    TYPE(pair), DIMENSION(:),INTENT(in) :: array1
    LOGICAL,INTENT(in) :: opt1
    LOGICAL,INTENT(in),OPTIONAL :: opt2

    IF(opt1) THEN
       is_sorted_pair = is_sorted_int(array1%key,opt2)
    ELSE
       is_sorted_pair = is_sorted_real(array1%val,opt2)
    END IF
  END FUNCTION is_sorted_pair


END MODULE list_tools
