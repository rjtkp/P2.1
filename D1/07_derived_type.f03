PROGRAM TYPE
  USE list_tools
  IMPLICIT NONE

  TYPE(pair) , ALLOCATABLE :: t(:)
  INTEGER (4):: n
  REAL(4) :: x, checksum, my_sum, tolerance

  tolerance = 1.e-5

  READ(5,*) n         !reading the number of integers
  WRITE(*,*) "Total number of integers", n
  ALLOCATE(t(n))
  READ(5,*)t          ! so, read them into the array t
  READ(5,*) checksum  ! the provided sum of the numbers

  my_sum = SUM(t(:)%val)
  x = ABS(my_sum - checksum)
  IF (x .LE. tolerance) THEN
     WRITE(*,*) "my_sum:",my_sum ,"  =  ","givensum:", checksum
     WRITE(*,*) "The sum is correct :)"
     WRITE(*,*) "=============================================="
  ELSE
     WRITE(*,*) "the sum is not matching. The absolute difference is:", x
  ENDIF

  WRITE(*,*) "ascending by value ",is_sorted_pair(t,.FALSE.,.TRUE.)
  WRITE(*,*) "descending by value ",is_sorted_pair(t,.FALSE.,.FALSE.)
  WRITE(*,*) "descending by key ",is_sorted_pair(t,.TRUE.,.FALSE.)
  WRITE(*,*) "ascending by key ",is_sorted_pair(t,.TRUE.,.TRUE.)

  DEALLOCATE(t)
END PROGRAM TYPE
