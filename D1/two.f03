PROGRAM one
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, sp =4
  REAL (KIND= sp), ALLOCATABLE :: t(:)
  REAL (KIND= sp):: checksum , my_sum, tolerance,x  ! the real is 4byte long but has been convert to double presicion type with kind =8
  INTEGER (KIND=sp):: i, n

  OPEN(UNIT=11, FILE="./data/d2_1.dat")
  OPEN(UNIT=12, FILE="./data/d2_2.dat")
  OPEN(UNIT=13, FILE="./data/d2_3.dat")
  OPEN(UNIT=14, FILE="./data/d2_4.dat")
  OPEN(UNIT=15, FILE="./data/d2_5.dat")
  OPEN(UNIT=16, FILE="./data/d2_6.dat")

  tolerance =  0.00001_4

  DO i=11,16
     READ(i,*) n !reading the number of integers
     WRITE(*,*) "Total number of integers", n
     ALLOCATE( t(N) )
     READ(i,*)t          ! so, read them in
     READ(i,*) checksum
     my_sum = SUM(t)
     x = ABS(my_sum - checksum)
     IF (x .LE. tolerance) THEN
        WRITE(*,*) "my_sum:",my_sum ,"  =  ","givensum:", checksum
        WRITE(*,*) "It's working :)"
        WRITE(*,*) "=============================================="
     ELSE
        WRITE(*,*) "the absolute difference is:", x
     ENDIF
     DEALLOCATE(t)
     CLOSE(i)
  ENDDO

END PROGRAM one
