#!/bin/bash

./01_readint.x < ./data/d1_1.dat >> comp_sum_int
./01_readint.x < ./data/d1_2.dat >> comp_sum_int
./01_readint.x < ./data/d1_3.dat >> comp_sum_int
./01_readint.x < ./data/d1_4.dat >> comp_sum_int
./01_readint.x < ./data/d1_5.dat >> comp_sum_int
./01_readint.x < ./data/d1_6.dat >> comp_sum_int

./02_readfloat.x < ./data/d2_1.dat >> comp_sum_real
./02_readfloat.x < ./data/d2_2.dat >> comp_sum_real
./02_readfloat.x < ./data/d2_3.dat >> comp_sum_real
./02_readfloat.x < ./data/d2_4.dat >> comp_sum_real
./02_readfloat.x < ./data/d2_5.dat >> comp_sum_real
./02_readfloat.x < ./data/d2_6.dat >> comp_sum_real

./03_is_sorted.x < ./data/d1_1.dat >> comp_sort_int
./03_is_sorted.x < ./data/d1_2.dat >> comp_sort_int
./03_is_sorted.x < ./data/d1_3.dat >> comp_sort_int
./03_is_sorted.x < ./data/d1_4.dat >> comp_sort_int
./03_is_sorted.x < ./data/d1_5.dat >> comp_sort_int
./03_is_sorted.x < ./data/d1_6.dat >> comp_sort_int

./04_is_sorted_real.x < ./data/d2_1.dat >> comp_sort_real
./04_is_sorted_real.x < ./data/d2_2.dat >> comp_sort_real
./04_is_sorted_real.x < ./data/d2_3.dat >> comp_sort_real
./04_is_sorted_real.x < ./data/d2_4.dat >> comp_sort_real
./04_is_sorted_real.x < ./data/d2_5.dat >> comp_sort_real
./04_is_sorted_real.x < ./data/d2_6.dat >> comp_sort_real


./07_derived_type.x < ./data/d3_1.dat >> comp_sum_derived
./07_derived_type.x < ./data/d3_2.dat >> comp_sum_derived
./07_derived_type.x < ./data/d3_3.dat >> comp_sum_derived
./07_derived_type.x < ./data/d3_4.dat >> comp_sum_derived
./07_derived_type.x < ./data/d3_5.dat >> comp_sum_derived
./07_derived_type.x < ./data/d3_6.dat >> comp_sum_derived
