# Data Structures and Sorting Algorithms
## Sorting algorithms
During this course, I have coded for 6 different types of sorting algoritms. I will be presenting them from worst to best in terms of time taken to sort same arrays of size.

### Simple sort and Bubble sort
In simple sort we travel through array twice to where each compared with all other. Even if the array is sorted it will take same amount of time as the comparison operation are unavoidable in this algorithm. BUt swapping time is saved if the the array is already sorted. Bubble sort takes more time for unsorted arrays but is really fast for sorted arrays, as the bubble doesn't move at all. This can be observed from the timings. Simple sort performes better than bubble sort unsorted arrays.

```
Timing for simple sort

      100000 unsorted random   54.4086304     is sorted? T
      100000 already sorted   17.6113663     is sorted? T
      100000 mostly sorted   20.6664886     is sorted? T
      200000 unsorted random   182.581696     is sorted? T
      200000 already sorted   72.8283386     is sorted? T
      200000 mostly sorted   119.725525     is sorted? T
```
```
Timing for Bubble sort

      100000 unsorted random   103.209579     is sorted? T
      100000 already sorted   7.93457031E-04 is sorted? T
      100000 mostly sorted   32.8550110     is sorted? T
      200000 unsorted random   213.493362     is sorted? T
      200000 already sorted   6.40869141E-04 is sorted? T
      200000 mostly sorted   77.7982178     is sorted? T
```

### Insertion sort
In this algorithm, the number are sorted with each addition of a new number. The newly added number travels through the array till it reaches the right postion in the previously sorted part of the array. That's why it performs better if the array is partially or fully sorted. This can be observed from the timings.
```
      100000 unsorted random   10.5844707     is sorted? T
      100000 already sorted   7.28607178E-04 is sorted? T
      100000 mostly sorted   5.97381592E-03 is sorted? T
      200000 unsorted random   54.2982025     is sorted? T
      200000 already sorted   3.55529785E-03 is sorted? T
      200000 mostly sorted   1.91497803E-02 is sorted? T
```
### Quick Sort
The performance completely depends on the selected pivot. If the largest or the smallest number has been chosen, the performance goes to O(N^2). As we might, times encounter sorted or partialy sorted arrys, choosing the middle of the array can benificial.    
```
      100000 unsorted random   4.31659967E-02 is sorted? T
      100000 already sorted   2.13370025E-02 is sorted? T
      100000 mostly sorted   2.14489996E-02 is sorted? T
      200000 unsorted random   9.30590034E-02 is sorted? T
      200000 already sorted   5.28669953E-02 is sorted? T
      200000 mostly sorted   4.19370234E-02 is sorted? T
```
### Recursive Merge sort
Merge sort is one of the better algorithms available. In my code, I calling the function recursively and each time the buffer is half of the size previously taken. This happens until, the buffer merely of size two. Then I am comparing those two and fixing their position before merging with the neighbours.
```
         500 unsorted random   1.29999942E-04 is sorted? T
         500 already sorted   1.30000990E-05 is sorted? T
         500 mostly sorted   3.69999325E-05 is sorted? T
        1000 unsorted random   3.03999986E-04 is sorted? T
        1000 already sorted   2.19999347E-05 is sorted? T
        1000 mostly sorted   6.09999988E-05 is sorted? T
      100000 unsorted random   4.41720001E-02 is sorted? T
      100000 already sorted   2.82299519E-03 is sorted? T
      100000 mostly sorted   9.09999758E-03 is sorted? T
      200000 unsorted random   9.21909958E-02 is sorted? T
      200000 already sorted   4.91100550E-03 is sorted? T
      200000 mostly sorted   1.77450031E-02 is sorted? T
```

### Hybrid Sort of Merge and insertion sort
In this algorithm, I am recusively calling the function to devide the array until the buffer becomes of size 32. Then using insertion sort, I sort the neighbouring buffers before merging them using merge sort. Well scaling algorithms like merge sort often have significant overhead but that's not true for algorthms like insertion sort. We can observe that in lower range, the timing is comparable but in higher range the difference is visible.

```
         500 unsorted random   1.36000104E-04 is sorted? T
         500 already sorted   1.99999195E-05 is sorted? T
         500 mostly sorted   4.79998998E-05 is sorted? T
        1000 unsorted random   2.71999976E-04 is sorted? T
        1000 already sorted   2.79999804E-05 is sorted? T
        1000 mostly sorted   6.09999988E-05 is sorted? T
      100000 unsorted random   6.52040020E-02 is sorted? T
      100000 already sorted   4.69599664E-03 is sorted? T
      100000 mostly sorted   1.15340054E-02 is sorted? T
      200000 unsorted random  0.103072986     is sorted? T
      200000 already sorted   5.08299470E-03 is sorted? T
      200000 mostly sorted   1.43209994E-02 is sorted? T
```




