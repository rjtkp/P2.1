MODULE sorting
  USE list_tools
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: simplesort, quicksort, bubble_sort, insertion_sort, recursive_merge_sort, Hybrid_Merge_Sort
CONTAINS

  SUBROUTINE insertion_sort(arr)
    IMPLICIT NONE
    INTEGER :: n, i, j
    REAL(4), INTENT(inout):: arr(:)
    REAL(4):: x1
    n = SIZE(arr)

    DO i = 2, n
       x1 = arr(i)
       j = i - 1
       DO WHILE (j >= 1)
          IF (arr(j) <= x1) EXIT
          arr(j + 1) = arr(j)
          j = j - 1
       END DO
       arr(j + 1) = x1
    END DO
  END SUBROUTINE insertion_sort

  SUBROUTINE bubble_sort(arr) !RESULT(arr)
    IMPLICIT NONE
    REAL(4), INTENT(inout):: arr(:)
    INTEGER(4):: j, bubble, en
    REAL(4) :: temp
    en = SIZE(arr)
    DO WHILE (en > 1)
       bubble = 0 !bubble in the greatest element out of order
       DO j = 1, (en-1)
          IF (arr(j) > arr(j+1)) THEN
             temp = arr(j)
             arr(j) = arr(j+1)
             arr(j+1) = temp
             bubble = j
          ENDIF
       ENDDO
       en = bubble
    ENDDO
  END SUBROUTINE bubble_sort

  ! pathetically bad sorting algorithm:
  ! loop over all unique pairs and swap the values
  ! if the left element is larger than the right one.
  SUBROUTINE simplesort(dat)
    IMPLICIT NONE
    REAL,DIMENSION(:),INTENT(inout) :: dat
    INTEGER :: num, i, j
    REAL :: tmp

    num = SIZE(dat,1)
    IF (num < 2) RETURN
    DO i=1,num-1
       DO j=i+1,num
          IF (dat(i) > dat(j)) THEN
             tmp = dat(i)
             dat(i) = dat(j)
             dat(j) = tmp
          END IF
       END DO
    END DO
  END SUBROUTINE simplesort

  ! quicksort implementation via recursion
  ! top-level takes whole array, recursions work on subsets.
  ! pick pivot element and recursively sort the two sublists.
  SUBROUTINE quicksort(dat)
    IMPLICIT NONE
    REAL,DIMENSION(:),INTENT(inout) :: dat
    INTEGER :: num, p

    num = SIZE(dat,1)
    IF (num < 2) RETURN

    p = select_pivot(dat,1,num)
    CALL quicksort_recurse(dat,1,p-1)
    CALL quicksort_recurse(dat,p+1,num)
  END SUBROUTINE quicksort

  RECURSIVE SUBROUTINE quicksort_recurse(dat,left,right)
    IMPLICIT NONE
    REAL,DIMENSION(:),INTENT(inout) :: dat
    INTEGER,INTENT(in) :: left, right
    INTEGER :: p

    IF (left < right) THEN
       p = select_pivot(dat,left,right)
       CALL quicksort_recurse(dat,left,p-1)
       CALL quicksort_recurse(dat,p+1,right)
    END IF
  END SUBROUTINE quicksort_recurse

  ! core step in quicksort. pick pivot value. then swap
  ! array elements so that smaller values are to the left of
  ! it and all larger values to the right. store pivot in
  ! the remaining spot. this element is now in its final location.
  ! return the index of the pivot element.
  ! The choice of the pivot is arbitrary, but crucial for getting
  ! good performance with presorted data.
  RECURSIVE FUNCTION select_pivot(dat,left,right) RESULT(i)
    IMPLICIT NONE
    REAL,DIMENSION(:),INTENT(inout) :: dat
    INTEGER :: i, j, right, left
    REAL :: tmp, pivot

    ! this is the classic choice of pivot element, assuming random data
    ! pivot = dat(right)
    ! an element in the middle is a much better choice for presorted data
    pivot = dat((left+right)/2)
    i = left
    DO j=left,right-1
       IF (pivot > dat(j)) THEN
          tmp = dat(i)
          dat(i) = dat(j)
          dat(j) = tmp
          i = i+1
       END IF
    END DO
    dat(right) = dat(i)
    dat(i) = pivot
  END FUNCTION select_pivot



  SUBROUTINE MERGE(arr1,arr2,arr3)

    REAL(4), INTENT(inout) :: arr1(:), arr3(:)
    REAL(4), INTENT(in)     :: arr2(:)

    INTEGER:: n1, n2, n3
    INTEGER :: i,j,k

    n1 = SIZE(arr1)
    n2 = SIZE(arr2)
    n3 = SIZE(arr3)

    i = 1; j = 1; k = 1;
    DO WHILE(i <= n1 .AND. j <= n2)
       IF (arr1(i) <= arr2(j)) THEN
          arr3(k) = arr1(i)
          i = i+1
       ELSE
          arr3(k) = arr2(j)
          j = j+1
       ENDIF
       k = k + 1
    ENDDO
    DO WHILE (i <= n1)
       arr3(k) = arr1(i)
       i = i + 1
       k = k + 1
    ENDDO
    RETURN

  ENDSUBROUTINE MERGE

  RECURSIVE SUBROUTINE recursive_merge_sort(arr1,n,T)
    INTEGER, INTENT(in) :: n
    REAL(4), INTENT(inout) :: arr1(N)
    REAL(4), INTENT (out) :: T((N+1)/2)
    REAL :: x

    INTEGER :: n1, n2

    IF (n < 2) RETURN
    IF (n == 2) THEN
       IF (arr1(1) > arr1(2)) THEN
          x = arr1(1)
          arr1(1) = arr1(2)
          arr1(2) = x
       ENDIF
       RETURN
    ENDIF
    n1=(N+1)/2
    n2=n-n1

    CALL recursive_merge_sort(arr1,n1,T)
    CALL recursive_merge_sort(arr1(n1+1),n2,T)

    IF (arr1(n1) > arr1(n1+1)) THEN
       T(1:n1)=arr1(1:n1)
       CALL MERGE(T,arr1(n1+1:n),arr1)
    ENDIF
    RETURN

  END SUBROUTINE Recursive_Merge_Sort

  RECURSIVE SUBROUTINE hybrid_merge_sort(arr1,n,T)
    INTEGER, INTENT(in) :: n
    REAL(4), INTENT(inout) :: arr1(N)
    REAL(4), INTENT (out) :: T((N+1)/2)

    INTEGER :: n1, n2

    IF (n < 32) THEN
       CALL insertion_sort(arr1)
       RETURN
    ENDIF
    IF (n == 32) THEN
       CALL insertion_sort(arr1)
       RETURN
    ENDIF
    n1=(N+1)/2
    n2=n-n1

    CALL recursive_merge_sort(arr1,n1,T)
    CALL recursive_merge_sort(arr1(n1+1),n2,T)

    IF (arr1(n1) > arr1(n1+1)) THEN
       T(1:n1)=arr1(1:n1)
       CALL MERGE(T,arr1(n1+1:n),arr1)
    ENDIF
    RETURN

  END SUBROUTINE Hybrid_Merge_Sort

END MODULE sorting
