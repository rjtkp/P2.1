PROGRAM bubble
  USE sorting
  USE list_tools
  IMPLICIT NONE

  INTEGER :: num, i, n
  REAL,ALLOCATABLE,DIMENSION(:) :: dat
  REAL :: time1, time2, rv
  INTEGER,PARAMETER,DIMENSION(9) :: sizes = (/ &
       500,1000,2000,5000,10000,20000,50000,100000,200000 /)
  ! 50,100,200,500,1000,2000,5000,10000,20000 /)

  ! initialize pseudo random number generator
  CALL RANDOM_SEED()

  ! loop over various pre-defined array dimensions for sorting benchmarks.
  DO n=1,SIZE(sizes,1)
     num = sizes(n)
     ALLOCATE(dat(num))

     ! fill array with uniform distributed random numbers
     DO i=1,num
        CALL RANDOM_NUMBER(rv)
        dat(i) = (ISHFT(HUGE(i),-7)*0.000001*rv)
     END DO

     ! call sort algorithm and measure the time spent on it.
     CALL CPU_TIME(time1)
     CALL bubble_sort(dat)
     CALL CPU_TIME(time2)
     IF (.NOT. is_sorted_real(dat)) THEN
        WRITE(*,*) 'WARNING: array is not sorted! Size =', num
     END IF
     WRITE(*,*) num, 'unsorted random', time2-time1, "is sorted?",is_sorted_real(dat)

     ! call sort again on the already sorted data
     CALL CPU_TIME(time1)
     CALL bubble_sort(dat)
     CALL CPU_TIME(time2)
     IF (.NOT. is_sorted_real(dat)) THEN
        WRITE(*,*) 'WARNING: array is not sorted! Size =', num
     END IF
     WRITE(*,*) num, 'already sorted', time2-time1, "is sorted?",is_sorted_real(dat)

     ! swap a few elements of the sorted array and sort one more time
     CALL swap(dat,INT(LOG(REAL(num))))
     CALL CPU_TIME(time1)
     CALL bubble_sort(dat)
     CALL CPU_TIME(time2)
     IF (.NOT. is_sorted_real(dat)) THEN
        WRITE(*,*) 'WARNING: array is not sorted! Size =', num
     END IF
     WRITE(*,*) num, 'mostly sorted', time2-time1, "is sorted?",is_sorted_real(dat)

     ! release storage
     DEALLOCATE(dat)
  END DO

END PROGRAM bubble
