#!/bin/bash

./11_insertion_sort.x > insertionsort
./12_quicksort.x > quick_sort_with_middle_pivot
./13_recursive_merge.x > recursive_mergesort
./14_hybrid_sort.x > hybrid_mergesort
./09_simplesort.x  > simplesort
./10_bubblesort.x  > bubblesort


cat insertionsort | awk '{print $1,$4}' > plot_insertion
cat quick_sort_with_middle_pivot | awk '(NR%2 == 0){print $1,$4}' > plot_sorted_choosing_middile_pivot
cat bubblesort | awk '{print $1,$4}' > plot_bubblesort
cat simplesort | awk '(NR%3 == 0) {print $1,$4}' > plot_simplesort
cat quick_sort_with_middle_pivot | awk '(NR%2 == 1){print $1,$4}' > plot_unsorted_choosing_middile_pivot
cat recursive_mergesort | awk '{print $1,$4}' > plot_recursive_merge
cat hybrid_mergesort | awk '{print $1,$4}' > plot_hybrid_mergesort
