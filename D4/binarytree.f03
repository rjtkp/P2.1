MODULE tree_funcs
  IMPLICIT NONE

  TYPE, PUBLIC :: pair
     INTEGER (4):: Key
     REAL(4) :: val
  END TYPE Pair

  TYPE type_a
     TYPE(pair) :: p
     TYPE(type_a), POINTER :: left => NULL()
     TYPE(type_a), POINTER :: right => NULL()
  END TYPE type_a

CONTAINS

  RECURSIVE SUBROUTINE add_node(root, node)
    TYPE(type_a), POINTER, INTENT(inout):: root
    TYPE(type_a), POINTER,INTENT(in) :: node

    IF(.NOT.ASSOCIATED(root)) THEN
       root => node
    ELSE IF(node%p%key <= root%p%key) THEN
       CALL add_node(root%left, node)
    ELSE
       CALL add_node(root%right, node)
    END IF
  END SUBROUTINE add_node

  RECURSIVE FUNCTION find (bt, k) RESULT(fp)
    IMPLICIT NONE
    TYPE(type_a), POINTER, INTENT(IN) :: bt
    INTEGER(4), INTENT(IN) :: k
    TYPE(pair) :: fp
    IF (k == bt%p%key) THEN
       fp%key = bt%p%key
       fp%val = bt%p%val
    ELSE IF (k < bt%p%key) THEN
       IF (.NOT. ASSOCIATED(bt%left)) THEN
          PRINT*, 'WARNING: k not found'
       ELSE
          fp = find(bt%left, k)
       END IF
    ELSE IF (k > bt%p%key) THEN
       IF (.NOT. ASSOCIATED(bt%right)) THEN
          PRINT*, 'WARNING: k not found'
       ELSE
          fp = find(bt%right, k)
       END IF
    END IF
  END FUNCTION find



  RECURSIVE SUBROUTINE print_tree(root)
    TYPE(type_a), POINTER :: root
    IF(.NOT.ASSOCIATED(root)) THEN
       WRITE(*,'(a)') ' Empty tree'
    ELSE
       IF(ASSOCIATED(root%left)) CALL print_tree(root%left)
       WRITE(*,*) root%p%key
       IF(ASSOCIATED(root%right)) CALL print_tree(root%right)
    END IF
  END SUBROUTINE print_tree

  RECURSIVE SUBROUTINE delete(root)
    IMPLICIT NONE
    TYPE(type_a), POINTER, INTENT(INOUT) :: root
    IF (ASSOCIATED(root%left)) THEN
       CALL delete(root%left)
    END IF
    IF (ASSOCIATED(root%right)) THEN
       CALL delete(root%right)
    END IF
    DEALLOCATE(root)
  END SUBROUTINE delete


END MODULE tree_funcs


PROGRAM binary_tree
   USE tree_funcs
   REAL harvest
   INTEGER i
   ! TYPE(type_a), TARGET :: temp
   TYPE(type_a), POINTER :: root => NULL()
   TYPE(type_a), POINTER :: ptr => NULL()
   TYPE(pair) :: p1
   REAL :: time1, time2
   INTEGER many

   WRITE(*,'(a)', advance = 'no') ' Enter how many elements you want:> '
   READ(*,*) many
   CALL random_SEED()
   CALL CPU_TIME(time1)
   DO i = 1, many
      CALL random_NUMBER(harvest)
      p1%val = harvest*100000
      p1%key = INT(p1%val)
      ALLOCATE(ptr)
      ptr = type_a(p1, NULL(), NULL())
      CALL add_node(root, ptr)
   END DO
  CALL CPU_TIME(time2)
  WRITE(*,*)'to create the tree time taken',(time2-time1)
   ! WRITE(*,'(a)') ' Printing the tree...'
   ! CALL print_tree(root)
   count = 0
   sum = 0
   ! temp = type_a(find_n(root, count/2), NULL(), NULL())
   ! CALL find(root, 4)
   CALL delete(root)
END PROGRAM binary_tree
