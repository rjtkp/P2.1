#!/bin/bash

# ./16_next_prime.x
./17_lookup_linklist.x < ./data/d3_1.dat >> lookup_linklist
./17_lookup_linklist.x < ./data/d3_2.dat >> lookup_linklist
./17_lookup_linklist.x < ./data/d3_3.dat >> lookup_linklist
./17_lookup_linklist.x < ./data/d3_4.dat >> lookup_linklist
./17_lookup_linklist.x < ./data/d3_5.dat >> lookup_linklist
./17_lookup_linklist.x < ./data/d3_6.dat >> lookup_linklist
./19_array_stack.x >> array_stack
./20_stack_ll.x >> stack_linklist
